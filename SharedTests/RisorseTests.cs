﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Shared.Tests
{
    [TestClass()]
    public class RisorseTests
    {

        /// <summary>
        /// Testa la risorsa DizionarioItaliano
        /// </summary>
        [TestMethod()]
        public void DizionarioItalianoTest()
        {
            // Dizionario presente
            Assert.IsNotNull(Risorse.DizionarioItaliano);

            List<string> dizionario = new List<string>(Risorse.DizionarioItaliano.Split(Environment.NewLine));

            // Contiene almeno 1.000 parole
            Assert.IsTrue(dizionario.Count > 1000);

            // Qualche semplice controllo sul contenuto
            Assert.IsTrue(dizionario.Contains("albero"));
            Assert.IsTrue(dizionario.Contains("metallo"));
            Assert.IsTrue(dizionario.Contains("zucchero"));
        }


    }
}