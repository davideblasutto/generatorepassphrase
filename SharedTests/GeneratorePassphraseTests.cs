﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Tests
{
    [TestClass()]
    public class GeneratorePassphraseTests
    {
        private const char Separatore = '-';

        /// <summary>
        /// Ripete 1000 volte l'azione richiesta
        /// </summary>
        private void Ripeti(Action azione)
        {
            for (int i = 0; i < 1000; i++)
            {
                azione();
            }
        }

        /// <summary>
        /// Testa l'uso di un dizionario personalizzato
        /// </summary>
        [TestMethod()]
        public void DizionarioTest()
        {
            List<string> dizionarioPersonalizzato = new List<string>() { "jsdlkjsdlkjsalk", "pgyojpgfylft", "qplxhceelgp" };
            GeneratorePassphrase generatore = new GeneratorePassphrase(dizionarioPersonalizzato)
                .LunghezzaMinimaComplessiva(3)
                .LunghezzaMassimaComplessiva(100);

            Ripeti(() =>
            {
                string passphrase = generatore.Genera();
                Console.WriteLine(passphrase);

                // La passphrase contiene una delle parole del dizionario personalizzato
                Assert.IsTrue(dizionarioPersonalizzato.Any(p => passphrase.Contains(p)));
            });

        }

        /// <summary>
        /// Testa i parametri LunghezzaMinimaComplessiva e LunghezzaMassimaComplessiva
        /// </summary>
        [TestMethod()]
        public void LunghezzaTest()
        {
            GeneratorePassphrase generatore = new GeneratorePassphrase()
                .LunghezzaMinimaComplessiva(10)
                .LunghezzaMassimaComplessiva(20);

            Ripeti(() =>
            {
                string passphrase = generatore.Genera();
                Console.WriteLine(passphrase);

                // Lunghezza nel range indicato
                Assert.IsTrue(passphrase.Length >= 10 && passphrase.Length <= 20);
            });
        }

        /// <summary>
        /// Testa il parametro Separatore
        /// </summary>
        [TestMethod()]
        public void SeparatoreTest()
        {
            GeneratorePassphrase generatore = new GeneratorePassphrase()
                .Separatore(Separatore);

            Ripeti(() =>
            {
                string passphrase = generatore.Genera();
                Console.WriteLine(passphrase);

                // Separatore presente
                Assert.IsTrue(passphrase.Contains(Separatore));
            });
        }

        /// <summary>
        /// Testa parametri incompatibili
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void ParametriIncompatibiliTest()
        {
            GeneratorePassphrase generatore = new GeneratorePassphrase()
                .NumeroParole(8)
                .Separatore(Separatore)
                .LunghezzaMassimaComplessiva(12); // Con i parametri precedenti non verrà mai generata una passphrase così corta

            Ripeti(() =>
            {
                // Dovrebbe sollevare un'exception
                generatore.Genera();
            });
        }

        /// <summary>
        /// Testa il parametro Legacy
        /// </summary>
        [TestMethod()]
        public void LegacyTest()
        {
            GeneratorePassphrase generatore = new GeneratorePassphrase()
                .Legacy();

            Ripeti(() =>
            {
                string passphrase = generatore.Genera();
                Console.WriteLine(passphrase);

                List<char> caratteri = new List<char>();
                caratteri.AddRange(passphrase);

                // Deve contenere almeno un carattere non-lettera/non-numero 
                Assert.IsTrue(caratteri.Any(c => !char.IsLetterOrDigit(c)));

                // Deve contenere almeno un carattere maiuscolo
                Assert.IsTrue(passphrase.ToLower() != passphrase);
            });
        }

        /// <summary>
        /// Testa il parametro NumeroCifreTraParole
        /// </summary>
        [TestMethod()]
        public void NumeroCifreTraParoleTest()
        {
            GeneratorePassphrase generatore = new GeneratorePassphrase()
                .NumeroCifreTraParole(2);

            Ripeti(() =>
            {
                string passphrase = generatore.Genera();
                Console.WriteLine(passphrase);

                List<char> caratteri = new List<char>();
                caratteri.AddRange(passphrase);

                // Deve contenere almeno un carattere cifra
                Assert.IsTrue(caratteri.Any(c => char.IsDigit(c)));
            });
        }

        /// <summary>
        /// Testa i parametri NumeroParole e LunghezzaMinimaParole
        /// </summary>
        [TestMethod()]
        public void ParoleTest()
        {
            const int numeroParole = 4;
            const int lunghezzaMinimaParole = 10;

            GeneratorePassphrase generatore = new GeneratorePassphrase()
                .NumeroParole(numeroParole)
                .LunghezzaMinimaParole(lunghezzaMinimaParole)
                .Separatore(Separatore);

            Ripeti(() =>
            {
                string passphrase = generatore.Genera();
                Console.WriteLine(passphrase);
                List<string> parole = new List<string>(passphrase.Split(Separatore));

                // Numero di parole indicato
                Assert.IsTrue(parole.Count == numeroParole);

                // Nessuna delle parole è più corta del previsto
                // TODO: A volte succede
                Assert.IsFalse(parole.Any(p => p.Length < lunghezzaMinimaParole));
            });


        }
    }
}