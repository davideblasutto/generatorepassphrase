using BlazorStrap;
using Frontend.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Syncfusion.Blazor;
using System;
using System.IO;
using System.Reflection;

namespace Frontend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSyncfusionBlazor();
            services.AddRazorPages();
            services.AddServerSideBlazor();

            // BlazorStrap
            services.AddBootstrapCss();

            // Servizi Scoped - Un'istanza per ogni client collegato
            services.AddScoped<Js>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzAyNDY4QDMxMzgyZTMyMmUzMERuOElwN1g4K1ZoNUw2Nys3K2FLdnEvWENrL2dIWENIeXJncCtaaTU4dW89");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // In sviluppo gli asset statici di Syncfustion.Blazor e BlazorStrap vengono caricati automaticamente da %userprofile%\.nuget\packages\syncfusion.blazor\[versione]
            }
            else
            {
                app.UseExceptionHandler("/Error");

                // In produzione � necessario configurare un provider per gli asset statici di Syncfusion.Blazor e BlazorStrap
                string percorsoAssetStatici = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "AssetStatici");
                Console.WriteLine($"Percorso asset statici: {percorsoAssetStatici}");
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(percorsoAssetStatici),
                    RequestPath = "/_content"
                });
            }

            // Se � attivo l'HTTPS fa il redirect da HTTP -> HTTPS
            // TODO: Funziona?
            if (ImpostazioniFrontend.UtilizzaHttps)
            {
                app.UseHttpsRedirection();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
