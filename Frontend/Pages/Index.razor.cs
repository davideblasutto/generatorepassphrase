﻿using Frontend.Utility;
using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Lists;
using Syncfusion.Blazor.Notifications;
using Shared.Utility.Estensioni;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Frontend.Pages
{
    public class IndexBase : ComponentBase
    {
        #region Dependency Injection

        [Inject] public Js Js { get; set; }

        #endregion

        protected Configurazione Config = new Configurazione()
        {
            Quante = 5,
            NumeroCifreTraParole = 2
        };

        protected ClientBackend ClientBackend = ClientBackend.GetClient();

        protected ObservableCollection<string> PassphraseGenerate = new ObservableCollection<string>();

        #region Elementi UI

        protected SfToast Toast;
        protected SfListView<string> ListViewPassphraseGenerate;

        #endregion

        #region Binding UI

        protected string VersioneBackend, VersioneFrontend;

        #endregion

        #region Override di ComponentBase

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (firstRender)
            {
                // Versioni
                VersioneFrontend = Assembly.GetExecutingAssembly().GetName().Version.SemVer();
                VersioneBackend = await ClientBackend.GetVersione();
                StateHasChanged();
            }

        }

        #endregion

        /// <summary>
        /// Al click su "Genera"
        /// </summary>
        protected async void GeneraPassphrase()
        {
            PassphraseGenerate.Sostituisci(await ClientBackend.GeneraPassphrase(Config));
            StateHasChanged();
        }

        /// <summary>
        /// Alla selezione di una password la copia
        /// </summary>
        protected async void CopiaPassphrase()
        {
            // Copia la passphrase
            string passphraseDaCopiare = (await ListViewPassphraseGenerate.GetSelectedItems()).Text.FirstOrDefault();
            Js.CopiaInClipboard(passphraseDaCopiare);

            // Lo comunica all'utente
            await Toast.Show(new ToastModel
            {
                Content = "Passphrase copiata negli appunti.",
                CssClass = "e-toast-success",
                Icon = "e-clipboard"
            });
        }
    }
}
