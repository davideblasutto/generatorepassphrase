﻿/**
 * Funzioni Javascript
 * Nota: È importante che i metodi abbiano lo stesso identico nome della loro controparte C# in Utility.Js
 */

/**
 * Copia un testo nella clipboard
 * Preso da: https://www.syncfusion.com/kb/10358/how-to-create-a-pdf-file-in-blazor-using-c
 */
function CopiaInClipboard(testo) {

    // Prova a eseguire l'azione
    navigator.clipboard.writeText(testo).then(function () {
        console.log('Copiato negli appunti:', testo)
    }, function (errore) {
        console.log('Errore:', errore)
    });

}
