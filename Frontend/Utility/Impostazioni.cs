﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration; 

namespace Frontend
{
    /// <summary>
    /// Impostazioni del frontend
    /// </summary>
    public static class ImpostazioniFrontend
    {
        private static readonly IConfigurationSection impostazioni = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection(nameof(ImpostazioniFrontend));

        public static bool UtilizzaHttps => Convert.ToBoolean(impostazioni[nameof(UtilizzaHttps)]);
        public static int Porta => Convert.ToInt32(impostazioni[nameof(Porta)]);
        public static string UriBackend => impostazioni[nameof(UriBackend)];
        public static string Certificato => impostazioni[nameof(Certificato)];
        public static string PasswordCertificato => impostazioni[nameof(PasswordCertificato)];
    }
}
