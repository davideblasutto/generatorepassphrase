﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Frontend.Utility
{
    /// <summary>
    /// Classe per gestire il file Javascript script.js
    /// </summary>
    public class Js
    {
        // Dependecy Injection: IJSRuntime
        private readonly IJSRuntime jsRuntime;
        public Js(IJSRuntime jsRuntime)
        {
            this.jsRuntime = jsRuntime;
        }

        /// <summary>
        /// Copia un testo nella clipboard
        /// </summary>
        /// <remarks>Può sollevare delle exception</remarks>
        /// <param name="testo">Testo da copiare</param>
        public void CopiaInClipboard(string testo) => jsRuntime.InvokeVoidAsync(nameof(CopiaInClipboard), new object[] { testo });
    }
}
