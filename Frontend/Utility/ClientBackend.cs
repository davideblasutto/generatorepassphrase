﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Frontend.Utility
{
    /// <summary>
    /// Classe per gestire le comunicazioni con il backend
    /// </summary>
    public class ClientBackend
    {
        #region Costanti e proprietà statiche

        /// <summary>
        /// Timeout richieste HTTP
        /// </summary>
        public static readonly TimeSpan TimeoutRichieste = new TimeSpan(hours: 0, minutes: 0, seconds: 30);

        #endregion

        private readonly HttpClient clientHttp;

        #region Costuttori e metodi factory

        /// <summary>
        /// Restituisce un client per il backend pronto all'uso
        /// </summary>
        public static ClientBackend GetClient() => new ClientBackend();

        private ClientBackend()
        {
            // Istanzia il client HTTP generale
            clientHttp = GetClientHttp();
        }

        #endregion

        /// <summary>
        /// Fa generare al backend delle passphrase
        /// </summary>
        public async Task<IEnumerable<string>> GeneraPassphrase(Configurazione configurazione)
        {
            // Il backend si aspetta che i parametri siano sottoforma di "query" nell'URL
            string uriRichiesta = Varie.ComponiUriRichiesta("Passphrase", configurazione);

            // Invia richiesta
            var risposta = await clientHttp.GetAsync(uriRichiesta);

            // Se tutto è andato bene il backend restituisce 200 o 202 - Altrimenti solleva exception
            risposta.EnsureSuccessStatusCode();

            Console.WriteLine(await risposta.Content.ReadAsStringAsync());

            // Deserializza la risposta come lista di stringhe
            return JsonConvert.DeserializeObject<List<string>>(await risposta.Content.ReadAsStringAsync());
        }

        public async Task<string> GetVersione()
        {
            // Invia richiesta
            var risposta = await clientHttp.GetAsync("Versione");

            // Se tutto è andato bene il backend restituisce 200 - Altrimenti solleva exception
            risposta.EnsureSuccessStatusCode();

            // Deserializza come stringa, così da rimuovere le virgolette
            return JsonConvert.DeserializeObject<string>(await risposta.Content.ReadAsStringAsync());
        }

        #region Metodi interni

        public HttpClient GetClientHttp() =>
            // Configurazione base del client HTTPS
            new HttpClient()
            {
                BaseAddress = new Uri(GetUrlBase()),
                Timeout = TimeoutRichieste
            };

        public static string GetUrlBase() => $"{ImpostazioniFrontend.UriBackend}/api/v1/";

        #endregion
    }
}
