﻿using System.ComponentModel.DataAnnotations;

namespace Frontend
{
    public class Configurazione
    {
        [Required]
        public int? Quante { get; set; }
        public int? NumeroParole { get; set; }
        public int? NumeroCifreTraParole { get; set; }
        public int? LunghezzaMinimaParole { get; set; }
        public int? LunghezzaMinimaComplessiva { get; set; }
        public int? LunghezzaMassimaComplessiva { get; set; }
        public string? Separatore { get; set; }
        public bool? Legacy { get; set; }
    }
}
