﻿using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace Frontend.Utility
{
    public static class Varie
    {
        /// <summary>
        /// Compone l'URL per una richiesta HTTP, con l'oggetto parametri come parametri nell'URI stesso       
        /// </summary>
        /// <remarks>
        /// L'oggetto "parametri" può contenere proprietà enumerable, che vengono correttamente gestiti
        /// </remarks>
        public static string ComponiUriRichiesta(string endpoint, object parametri)
        {
            // Compone parametri
            NameValueCollection stringaParametri = HttpUtility.ParseQueryString(string.Empty);
            if (parametri != null)
            {
                foreach (PropertyInfo infoProprieta in parametri.GetType().GetProperties())
                {
                    // Se la proprietà è un enumerable (ma non string) la logica è diversa
                    if (infoProprieta.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)) && infoProprieta.PropertyType != typeof(string))
                    {
                        // Enumerable - Aggiunge tutti gli elementi
                        foreach (object valore in infoProprieta.GetValue(parametri) as IEnumerable)
                        {
                            // Per ogni valore nell'enumerable
                            stringaParametri.Add(infoProprieta.Name, GetStringaPerQueryUri(valore));
                        }
                    }
                    else
                    {
                        // Non enumerable - Aggiunge la proprietà, se non è null
                        var valore = infoProprieta.GetValue(parametri);
                        if (valore != null)
                        {
                            stringaParametri.Add(infoProprieta.Name, GetStringaPerQueryUri(valore));
                        }
                    }
                }
            }

            // Restituisce endpoint e parametri
            return $"{endpoint}?{stringaParametri}";
        }

        /// <summary>
        /// Serializza un valore per inserirlo come parametro in un URI
        /// </summary>
        public static string GetStringaPerQueryUri(object valore)
        {
            if (valore is DateTime time)
            {
                // Serializza la data/ora nel formato più standard possibile
                return time.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                return valore.ToString();
            }
        }
    }
}
