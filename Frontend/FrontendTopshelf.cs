﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection;
using Shared.Utility;
using Shared.Utility.Estensioni;
using Topshelf;
using System.Security.Cryptography.X509Certificates;

namespace Frontend
{
    /// <summary>
    /// Classe del servizio del frontend (gestita da Topshelf)
    /// </summary>
    public class FrontendTopshelf
    {
        private IHost webServer;

        /// <summary>
        /// Avvio del servizio
        /// </summary>
        /// <returns>Esito dell'avvio</returns>
        public bool Start()
        {
            try
            {
                Console.WriteLine($"{Assembly.GetExecutingAssembly().GetName().Name} versione {Assembly.GetExecutingAssembly().GetName().Version.SemVer()} avviato");

                // Compone l'URL base del frontend
                string urlBase = WebServer.ComponiUrlBaseWebServer(ImpostazioniFrontend.UtilizzaHttps, ImpostazioniFrontend.Porta);

                // Inizia ascolto
                try
                {
                    webServer = Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder().ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();

                        webBuilder.ConfigureKestrel(opzioniKestrel =>
                        {
                            // Configura ascolto su porta indicata tramite Kestrel
                            opzioniKestrel.ListenAnyIP(ImpostazioniFrontend.Porta, opzioniListen =>
                            {
                                // Certificato?
                                X509Certificate2 certificato = null;
                                if (!string.IsNullOrWhiteSpace(ImpostazioniFrontend.Certificato))
                                {
                                    // Carica il certificato dal percorso indicato
                                    certificato = new X509Certificate2(
                                        ImpostazioniFrontend.Certificato,
                                        ImpostazioniFrontend.PasswordCertificato,
                                        X509KeyStorageFlags.PersistKeySet
                                        );
                                }

                                // Se previsto ascolta sul protocollo HTTPS, altrimenti HTTP
                                if (ImpostazioniFrontend.UtilizzaHttps)
                                {
                                    opzioniListen.UseHttps(certificato);
                                }
                            });
                        });

                    }).Build();
                    _ = webServer.RunAsync();

                    Console.WriteLine($"Frontend in ascolto su porta {ImpostazioniFrontend.Porta}, protocollo {(ImpostazioniFrontend.UtilizzaHttps ? "HTTPS" : "HTTP")}");

                    return true;
                }
                catch (TargetInvocationException e)
                {
                    throw WebServer.GestisciTargetInvocationException(e, urlBase);
                }

            }
            catch (Exception e)
            {
                // Errore bloccante
                Console.WriteLine($"Errore bloccante durante l'avvio del servizio: {e.Message}");

                if (Program.Debug)
                {
                    // In debug lascia la console aperta
                    Console.ReadLine();
                }

                return false;
            }
        }

        /// <summary>
        /// Arresto del serivizio
        /// </summary>
        /// <returns>Esito dell'arresto</returns>
        public bool Stop()
        {
            try
            {
                Console.WriteLine($"Arresto del servizio {Assembly.GetExecutingAssembly().GetName().Name}...");

                // Termina il webserver
                Console.WriteLine($"Arresto del webserver...");
                webServer.Dispose();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Errore nell'arresto del serivizio: " + e.Message);
                Console.WriteLine("Inner Exception: " + e.InnerException?.Message);
                return false;
            }
        }
    }
}

