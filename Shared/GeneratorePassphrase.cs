﻿using System;
using System.Collections.Generic;

namespace Shared
{

    /// <summary>
    /// Generatore di passhrase in italiano, configurabile
    /// </summary>
    public class GeneratorePassphrase
    {

        #region Valori di default

        /// <summary>
        /// Valori di default per i parametri di Generatore Passphrase
        /// </summary>
        public static class Default
        {
            public const int NumeroParole = 2;
            public const int LunghezzaMinimaParole = 3;
            public const int LunghezzaMinimaComplessiva = 10;
            public const int LunghezzaMassimaComplessiva = 999;
            public const int NumeroCifreTraParole = 0;
            public const char Separatore = char.MinValue;
            public const bool Legacy = false;
        }

        #endregion

        #region Costanti

        private const char CifraFissa = '1';
        private const char CarattereSpecialeFisso = '_';
        private static readonly char CarattereNullo = char.MinValue;
        private const int NumeroMassimoTentativi = 5000;

        #endregion

        #region Parametri

        private readonly List<string> dizionario;
        private int numeroParole = Default.NumeroParole;
        private int lunghezzaMinimaParole = Default.LunghezzaMinimaParole;
        private int lunghezzaMinimaComplessiva = Default.LunghezzaMinimaComplessiva;
        private int lunghezzaMassimaComplessiva = Default.LunghezzaMassimaComplessiva;
        private int numeroCifreTraParole = Default.NumeroCifreTraParole;
        private char separatore = Default.Separatore;
        private bool legacy = Default.Legacy;

        #endregion

        #region Campi interni

        private readonly Random random = new Random();

        #endregion

        #region Costruttore e impostazione parametri

        /// <summary>
        /// Istanzia un generatore con il dizionario predefinito
        /// </summary>
        public GeneratorePassphrase()
        {
            // Dizionario predefinito (da risorse)
            dizionario = new List<string>(Risorse.DizionarioItaliano.Split(Environment.NewLine.ToCharArray()));

        }

        /// <summary>
        /// Istanzia un generatore con il dizionario indicato
        /// </summary>
        /// <param name="dizionario">Lista di parole da usare per la generazione</param>
        public GeneratorePassphrase(List<string> dizionario) => this.dizionario = dizionario;

        /// <summary>
        /// Rende le passphrase generate conformi ai criteri standard delle vecchie password (maiuscole/minuscole, cifre, caratteri speciali), senza renderle più difficili da memorizzare
        /// </summary>
        public GeneratorePassphrase Legacy()
        {
            this.legacy = true;
            return this;
        }

        /// <summary>
        /// Specifica il numero di parole che comporranno le passphrase generate 
        /// </summary>
        public GeneratorePassphrase NumeroParole(int numeroParole)
        {
            this.numeroParole = numeroParole;
            return this;
        }

        /// <summary>
        /// Specifica se e quante cifre inserire tra una parola e l'altra nelle passphrase generate
        /// </summary>
        public GeneratorePassphrase NumeroCifreTraParole(int numeroCifreTraParole)
        {
            this.numeroCifreTraParole = numeroCifreTraParole;
            return this;
        }

        /// <summary>
        /// Specifica la lunghezza minima delle parole che comporranno le passphrase generate
        /// </summary>
        public GeneratorePassphrase LunghezzaMinimaParole(int lunghezzaMinimaParole)
        {
            this.lunghezzaMinimaParole = lunghezzaMinimaParole;
            return this;
        }

        /// <summary>
        /// Specifica una lunghezza minima complessiva per le passphrase generate
        /// </summary>
        public GeneratorePassphrase LunghezzaMinimaComplessiva(int lunghezzaMinimaComplessiva)
        {
            this.lunghezzaMinimaComplessiva = lunghezzaMinimaComplessiva;
            return this;
        }

        /// <summary>
        /// Specifica una lunghezza massima complessiva per le passphrase generate
        /// </summary>
        public GeneratorePassphrase LunghezzaMassimaComplessiva(int lunghezzaMassimaComplessiva)
        {
            this.lunghezzaMassimaComplessiva = lunghezzaMassimaComplessiva;
            return this;
        }

        /// <summary>
        /// Specifica un separatore tra le parole che comporranno le passphrase generate 
        /// </summary>
        /// <remarks>
        /// Se tra le parole è stato specificato di inserire delle cifre allora il separatore verrà inserito sia prima che dopo le cifre
        /// </remarks>
        public GeneratorePassphrase Separatore(char separatore)
        {
            this.separatore = separatore;
            return this;
        }

        #endregion

        #region Generazione

        /// <summary>
        /// Genera una passphrase in base ai parametri ricevuti 
        /// </summary>
        /// <returns>Passphrase causale</returns>
        public string Genera()
        {
            int tentativi = 0;
            string passphrase;

            // Continua a generare passphrase finché non ne genera una della lunghezza (minima e massima) richiesta
            while (true)
            {
                // Dopo TOT tentativi si ferma: i parametri sono incompatibili
                if (tentativi > NumeroMassimoTentativi)
                {
                    throw new Exception($"Raggiunto il numero massimo di tentativi ({NumeroMassimoTentativi}) senza aver generato una passphrase: i parametri sono incompatibili.");
                }

                // Nuova passphrase
                tentativi++;
                passphrase = string.Empty;

                // Sequenza di parole
                for (int p = 1; p <= numeroParole; p++)
                {
                    // Aggiunge una parola a caso, della lunghezza minima richiesta
                    passphrase += GetParola(lunghezzaMinima: lunghezzaMinimaParole);

                    // Se non siamo alla fine aggiunge il separatore ed eventualmente delle cifre
                    if (p < numeroParole)
                    {
                        // Separatore, se necessario
                        if (separatore != CarattereNullo)
                        {
                            passphrase += separatore;
                        }
                        // Cifre, se necessario
                        if (numeroCifreTraParole > 0)
                        {
                            // Aggiunge il numero di cifre richieste
                            passphrase += GetCifre(numeroCifreTraParole);
                            // Aggiunge ulteriore separatore, se necessario
                            if (separatore != CarattereNullo)
                            {
                                passphrase += separatore;
                            }
                        }
                    }
                }

                // Legacy?
                if (legacy)
                {
                    // Prima lettera maiuscola
                    passphrase = passphrase.Substring(0, 1).ToUpper() + passphrase.Substring(1);
                    // Cifra, se non c'è già
                    if (numeroCifreTraParole == 0)
                    {
                        passphrase += CifraFissa;
                    }
                    // Carattere speciale, se il separatore non lo è già
                    if (separatore == CarattereNullo || char.IsLetterOrDigit(separatore))
                    {
                        // Il separatore è una lettera/numero, quindi non è un carattere speciale
                        // Ne aggiunge uno fisso per conformita legacy
                        passphrase += CarattereSpecialeFisso;
                    }
                }

                // Controlla lunghezza
                if (passphrase.Length >= lunghezzaMinimaComplessiva && passphrase.Length <= lunghezzaMassimaComplessiva)
                {
                    // La lunghezza va bene: restituisce la parola
                    return passphrase;
                }

            }
        }

        /// <summary>
        /// Restituisce una parola a caso dal dizionario, di lunghezza pari o superiore a quella richiesta
        /// </summary>
        /// <param name="lunghezzaMinima">Lunghezza minima della parola</param>
        /// <returns>Parola a caso</returns>
        private string GetParola(int lunghezzaMinima)
        {
            // Prende parole a caso finché non ne trova una abbastanza lunga
            while (true)
            {
                // Prende una parola a caso
                string parola = dizionario[random.Next(dizionario.Count)];

                // Controlla lunghezza
                if (parola.Length >= lunghezzaMinima)
                {
                    // La lunghezza è sufficiente: restituisce la parola
                    return parola;
                }
            }
        }

        /// <summary>
        /// Genera una stringa contenente il numero richieste di cifre casuali 
        /// </summary>
        /// <param name="lunghezza">Numero di cifre, aka lunghezza della stringa da generare</param>
        /// <returns>Stringa causale</returns>
        private string GetCifre(int lunghezza)
        {
            string cifre = string.Empty;

            for (int i = 0; i < lunghezza; i++)
            {
                cifre = string.Concat(cifre, random.Next(10).ToString());
            }

            return cifre;
        }

        #endregion
    }
}
