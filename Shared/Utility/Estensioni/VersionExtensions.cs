﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Utility.Estensioni
{
    /// <summary>
    /// Estensioni di Version
    /// </summary>
    public static class VersionExtensions
    {
        /// <summary>
        /// Stringa contenente la versione in formato versioning semantico (Major.Minor.Patch)
        /// </summary>
        public static string SemVer(this Version versione)
        {
            return $"{versione.Major}.{versione.Minor}.{versione.Build}";
        }
    }
}
