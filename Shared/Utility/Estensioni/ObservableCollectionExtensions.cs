﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Shared.Utility.Estensioni
{
    /// <summary>
    /// Estensioni di ObservableCollection&lt;T&gt;
    /// </summary>
    public static class ObservableCollectionExtensions
    {
        /// <summary>
        /// Sostituisce l'intera collection, senza reistanziarla, facendo partire tutti gli eventi del caso
        /// </summary>
        /// <typeparam name="T">Tipo degli elementi della collection</typeparam>
        /// <param name="observableCollectionAttuale">This</param>
        /// <param name="nuoviElementi">Nuovi elementi da inserire nella collection, al posto di quelli attuali</param>
        public static void Sostituisci<T>(this ObservableCollection<T> observableCollectionAttuale, IEnumerable<T> nuoviElementi)
        {
            // Elimina tutti gli elementi eventualmente presenti nella collection
            if (observableCollectionAttuale.Count > 0)
            {
                observableCollectionAttuale.Clear();
            }

            // Carica i nuovi elementi uno a uno
            foreach (T elemento in nuoviElementi)
            {
                observableCollectionAttuale.Add(elemento);
            }
        }
    }
}
