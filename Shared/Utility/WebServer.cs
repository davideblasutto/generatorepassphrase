﻿using System;
using System.Reflection;

namespace Shared.Utility
{
    /// <summary>
    /// Classe statica con metodi di suppporto per la gestione di un web server
    /// </summary>
    public static class WebServer
    {
        /// <summary>
        /// Compone un URL base per l'ascolto 
        /// </summary>
        public static string ComponiUrlBaseWebServer(bool utilizzaHttps, int porta) => $"http{(utilizzaHttps ? "s" : string.Empty)}://*:{porta}/";

        /// <summary>
        /// Risolleva una TargetInvocationException con un messaggio comprensibile, spiegando come prenotare l'URL 
        /// </summary>
        /// <param name="e">Exception sollevata</param>
        /// <param name="urlBase">URL base di ascolto del server</param>
        public static Exception GestisciTargetInvocationException(TargetInvocationException e, string urlBase)
        {
            // Ottiene il nome dell'utente Windows attuale
            string usernameWindows = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            // Risolleva l'exception con un messaggio comprensibile
            throw new Exception(
                $"Errore nell'avvio del web server.\r\n" +
                $"Il problema potrebbe essere risolto prenotando l'URL, eseguendo il seguente comando come amministratore:\r\n" +
                $"netsh http add urlacl url={urlBase} user={usernameWindows}",
                innerException: e
            );
        }

        public static string ComponiUrlBaseWebServer(object utilizzaHttps, object porta)
        {
            throw new NotImplementedException();
        }
    }
}
