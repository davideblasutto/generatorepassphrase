﻿using Shared;
using System.Collections.Generic;
using System.Web.Http;

namespace Backend
{
    /// <summary>
    /// Generatore di passphrase
    /// </summary>
    public class PassphraseController : ApiController
    {
        /// <summary>
        /// Genera delle passphrase
        /// </summary>
        /// <param name="quante">Numero di passphrase da generare (default: 1)</param>
        /// <param name="numeroParole">Numero di parole che comporranno le passphrase generate (default: 2)</param>
        /// <param name="numeroCifreTraParole">Cifre da inserire tra una parola e l'altra nelle passphrase generate (default: 0)</param>
        /// <param name="lunghezzaMinimaParole">Lunghezza minima delle parole che comporranno le passphrase generate (default: 3)</param>
        /// <param name="lunghezzaMinimaComplessiva">Lunghezza minima complessiva per le passphrase generate (default: 10)</param>
        /// <param name="lunghezzaMassimaComplessiva">Lunghezza massima complessiva per le passphrase generate (default: 999)</param>
        /// <param name="separatore">Separatore tra le parole che comporranno le passphrase generate (default: null)</param>
        /// <param name="legacy">Rende le passphrase generate conformi ai criteri standard delle vecchie password (maiuscole/minuscole, cifre, caratteri speciali), senza renderle più difficili da memorizzare (default: false)</param>
        /// <returns>Lista di passphrase</returns>
        public IEnumerable<string> Get(int quante = 1, int? numeroParole = null, int? numeroCifreTraParole = null, int? lunghezzaMinimaParole = null, int? lunghezzaMinimaComplessiva = null, int? lunghezzaMassimaComplessiva = null, char? separatore = null, bool legacy = false)
        {
            List<string> listaPassphrase = new List<string>();

            // Configura generatore
            GeneratorePassphrase generatore = new GeneratorePassphrase();

            if (numeroParole != null)
            {
                generatore.NumeroParole((int)numeroParole);
            }

            if (numeroCifreTraParole != null)
            {
                generatore.NumeroCifreTraParole((int)numeroCifreTraParole);
            }

            if (lunghezzaMinimaParole != null)
            {
                generatore.LunghezzaMinimaParole((int)lunghezzaMinimaParole);
            }

            if (lunghezzaMinimaComplessiva != null)
            {
                generatore.LunghezzaMinimaComplessiva((int)lunghezzaMinimaComplessiva);
            }

            if (lunghezzaMassimaComplessiva != null)
            {
                generatore.LunghezzaMassimaComplessiva((int)lunghezzaMassimaComplessiva);
            }

            if (separatore != null)
            {
                generatore.Separatore((char)separatore);
            }

            if (legacy)
            {
                generatore.Legacy();
            }



            for (int i = 0; i < quante; i++)
            {
                listaPassphrase.Add(generatore.Genera());
            }

            return listaPassphrase;
        }
    }
}
