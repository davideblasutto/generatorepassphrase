﻿using Owin;
using Swashbuckle.Application;
using System.Reflection;
using System.Web.Http;

namespace Backend
{
    public class Startup
    {
        /// <summary>
        /// This code configures Web API. The Startup class is specified as a type parameter in the WebApp.Start method.
        /// </summary>
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "GeneratorePassphrase",
                routeTemplate: "api/v1/{controller}"
            );

            // Swagger via Swashbuckle
            config.EnableSwagger(c =>
            {
                // API
                c.SingleApiVersion("v1", title: "GeneratorePassphrase");
                // Documentazione XML dei controller (dal progetto corrente)
                c.IncludeXmlComments(Assembly.GetExecutingAssembly().GetName().Name + ".xml");
            }).EnableSwaggerUi();

            appBuilder.UseWebApi(config);
        }
    }
}
