﻿using Microsoft.Owin.Hosting;
using System;
using System.Reflection;
using Topshelf;

namespace Backend
{
    /// <summary>
    /// Classe di avvio dell'eseguibile
    /// </summary>
    class Program
    {
        private const string NomeServizio = "GeneratorePassphraseBackend";

        /// <summary>
        /// Indica se il programma è stato compilato in debug oppure no.
        /// </summary>
        /// <remarks>
        /// Soluzione preferibile all'uso di <code>#if DEBUG</code> in quanto la presenza di porzioni di codice "irraggiungibile" crea problemi con l'analisi del codice stesso da parte di Visual Studio.
        /// </remarks>
#if DEBUG
        public static bool Debug = true;
#else
        public static bool Debug = false;
#endif

        /// <summary>
        /// Metodo di avvio dell'eseguibile 
        /// </summary>
        static void Main()
        {
            // Metadati servizio da assembly dell'eseguibile
            var assemblyName = Assembly.GetExecutingAssembly().GetName();

            // Configura host del servizio con Topshelf
            var host = HostFactory.New(c =>
            {

                // Classe del servizio
                c.Service<Backend>();

                // Metadati (da assembly, vedi sopra)
                c.SetDescription("Backend di Generatore passphrase");
                c.SetDisplayName(NomeServizio);
                c.SetServiceName(NomeServizio);
            });

            // Avvia host del servizio
            var exitCode = host.Run();

            // Exit code
            Environment.ExitCode = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
        }
    }
}
