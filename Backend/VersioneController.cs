﻿using System.Reflection;
using System.Web.Http;
using Shared.Utility.Estensioni;

namespace Backend
{
    /// <summary>
    /// Versione del backend
    /// </summary>
    public class VersioneController : ApiController
    {
        /// <summary>
        /// Versione del backend
        /// </summary>
        public string Get() => Assembly.GetExecutingAssembly().GetName().Version.SemVer();
    }
}
