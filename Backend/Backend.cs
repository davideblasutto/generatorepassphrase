﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Owin.Hosting;
using Backend.Properties;
using Topshelf;
using Shared.Utility.Estensioni;

namespace Backend
{
    /// <summary>
    /// Classe del servizio del backend (gestita da Topshelf)
    /// </summary>
    public class Backend : ServiceControl
    {
        private IDisposable webServer;

        /// <summary>
        /// Avvio del servizio
        /// </summary>
        /// <returns>Esito dell'avvio</returns>
        public bool Start(HostControl hostControl)
        {
            try
            {
                Console.WriteLine($"{Assembly.GetExecutingAssembly().GetName().Name} versione {Assembly.GetExecutingAssembly().GetName().Version.SemVer()} avviato");

                // URL base per l'ascolto
                string urlBase = Shared.Utility.WebServer.ComponiUrlBaseWebServer(Settings.Default.UtilizzaHttps, Settings.Default.Porta);

                // Inizia ascolto
                try
                {
                    webServer = WebApp.Start<Startup>(url: urlBase);

                    Console.WriteLine($"Backend in ascolto su URL base {urlBase}");
                    Console.WriteLine($" - API web disponibili all'URL {urlBase}api/v1");
                    Console.WriteLine($" - UI Swagger disponibile all'URL {urlBase}swagger");

                    return true;
                }
                catch (TargetInvocationException e)
                {
                    throw Shared.Utility.WebServer.GestisciTargetInvocationException(e, urlBase);
                }

            }
            catch (Exception e)
            {
                // Errore bloccante
                Console.WriteLine($"Errore bloccante durante l'avvio del servizio: {e.Message}");

                if (Program.Debug)
                {
                    // In debug lascia la console aperta
                    Console.ReadLine();
                }

                return false;
            }
        }

        /// <summary>
        /// Arresto del serivizio
        /// </summary>
        /// <returns>Esito dell'arresto</returns>
        public bool Stop(HostControl hostControl)
        {
            try
            {
                Console.WriteLine($"Arresto del servizio {Assembly.GetExecutingAssembly().GetName().Name}...");

                // Termina il webserver
                Console.WriteLine($"Arresto del webserver...");
                webServer.Dispose();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Errore nell'arresto del serivizio: " + e.Message);
                Console.WriteLine("Inner Exception: " + e.InnerException?.Message);
                return false;
            }
        }
    }
}
